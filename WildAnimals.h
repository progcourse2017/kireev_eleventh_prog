#ifndef WILDANIMALS_H
#define WILDANIMALS_H

#include "Zoo.h"

class WildAnimals : virtual public Zoo {
private:
    static unsigned quantity;

public:
    WildAnimals();
    WildAnimals(string _animal, string _name, bool _sex, double _price);
    WildAnimals(const WildAnimals &object);
    ~WildAnimals();

    inline unsigned getQuantity() { return quantity; };
    void counter();

    inline void setType(bool _type) { Zoo::type = _type; };
};


#endif //WILDANIMALS_H
