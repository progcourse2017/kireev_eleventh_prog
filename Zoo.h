#ifndef ZOO_H
#define ZOO_H

#include <string>

using namespace std;

class Zoo {
protected:
    string name, animal;
    bool sex;
    double price;
    static unsigned allCount;

    bool type;

public:
    Zoo();
    Zoo(string _animal, string _name, bool _sex, double _price);
    Zoo(const Zoo &object);
    virtual ~Zoo();

    virtual void setType(bool _type) = 0;

    inline void setAnimal(string _animal) { this->animal = _animal; };
    inline void setName(string _name) { this->name = _name; };
    inline void setSex(bool _sex) { this->sex = _sex; };
    inline void setPrice(double _price) { this->price = _price; };

    inline string getAnimal() { return animal; };
    inline string getName() { return name; };
    inline bool getSex() { return sex; };
    inline double getPrice() { return price; };
    inline unsigned getAllCount() { return allCount; };

    inline void changeParams(string _name) { this->setName(_name); };
    inline double changeParams(double _price) { this->setPrice(_price); };
};

#endif //ZOO_H
