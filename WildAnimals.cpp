#include "WildAnimals.h"

unsigned WildAnimals::quantity = 0;

WildAnimals::WildAnimals() {
    setType(0);
    counter();
}

WildAnimals::WildAnimals(string _animal, string _name,  bool _sex, double _price):
        Zoo(_animal, _name, _sex, _price) {
    setType(0);
    counter();
}

WildAnimals::WildAnimals(const WildAnimals &object):
        Zoo(object) {
    quantity = object.quantity;
}

WildAnimals::~WildAnimals() {}

void WildAnimals::counter() {
    WildAnimals::quantity++;
    Zoo::allCount++;
}