#include <iostream>
#include "Zoo.h"

unsigned Zoo::allCount = 0;

Zoo::Zoo() {}

Zoo::Zoo(string _animal, string _name,  bool _sex, double _price) {
    setAnimal(_animal);
    setName(_name);
    setSex(_sex);
    setPrice(_price);
}

Zoo::Zoo(const Zoo &object) {
    this->animal = object.animal;
    this->name = object.name;
    this->sex = object.sex;
    this->price = object.price;
    this->allCount = object.allCount;
    this->type = object.type;
}

Zoo::~Zoo() {}