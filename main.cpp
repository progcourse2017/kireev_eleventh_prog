#include <iostream>
#include <vector>
#include "Pets.h"
#include "WildAnimals.h"

using namespace std;

template <class C> C enter(C _animal);
template <class C> void print(C _animal);
template <class C> C changeParam(C _animal);

struct{
    string animal, name;
    double price;
} animals;

int main(int argc, char **argv) {
    vector<WildAnimals> dataWild;
    vector<Pets> dataPets;

    Pets objectPets("Dog", "Tima", true, 1000);
    dataPets.push_back(objectPets);

    WildAnimals objectWild("Wolf", "Max", true, 2000);
    dataWild.push_back(objectWild);

    //dataPets.push_back(objectPets);

    unsigned choice = 0;
    unsigned c,changeChoice;;
    while (choice != 4) {
        cout << "What do you want to do?\n"
                  << "1) Add a note\n"
                  << "2) Print all notes\n"
                  << "3) Fix params\n"
                  << "4) Quit the program\n"
                  << "Your choice: ";
        cin >> choice;
        cout << "\n";
        switch (choice) {
            case 1:
                cout << "1) Pet\n"
                     << "2) Wild animal\n";
                cin >> c;
                switch (c) {
                    case 1:
                        objectPets = enter(objectPets);
                        dataPets.push_back(objectPets);
                        objectPets.counter();
                        break;
                    case 2:
                        objectWild = enter(objectWild);
                        dataWild.push_back(objectWild);
                        objectWild.counter();
                        break;
                    default:
                        cout << "Error! Invalid input!\n";
                }
                break;
            case 2:
                if (dataPets.size() == 0)
                    cout << "Pets: empty\n";
                else {
                    cout << "Pets: \n";
                    for (unsigned count = 0; count < dataPets[0].getQuantity(); ++count) {
                        cout << count + 1 << ")\n";
                        print(dataPets[count]);
                    }
                }
                cout << "\n";
                if (dataWild.size() == 0)
                    cout << "WildAnimals: empty\n";
                else {
                    cout << "Wild animals: \n";
                    for (unsigned count = 0; count < dataWild[0].getQuantity(); ++count) {
                        cout << count + 1 << ")\n";
                        print(dataWild[count]);
                    }
                }
                break;
            case 3:
                cout << "1) Pet\n"
                     << "2) Wild animal\n";
                cin >> c;
                switch (c) {
                    case 1:
                        if (dataPets.size() == 0) {
                            cout << "Pets: empty\n";
                            break;
                        } else {
                            cout << "Pets: \n";
                            for (unsigned count = 0; count < dataPets[0].getQuantity(); ++count) {
                                cout << count + 1 << ")\n";
                                print(dataPets[count]);
                            }
                        }
                        changeChoice;
                        cout << "\nWhat kind of object do you want to change: ";
                        cin >> changeChoice;

                        dataPets[changeChoice - 1] = changeParam(dataPets[changeChoice - 1]);
                        break;
                    case 2:
                        if (dataWild.size() == 0) {
                            cout << "WildAnimals: empty\n";
                            break;
                        } else {
                            for (unsigned count = 0; count < dataWild[0].getQuantity(); ++count) {
                                cout << count + 1 << ")\n";
                                print(dataWild[count]);
                            }
                        }
                        changeChoice;
                        cout << "\nWhat kind of object do you want to change: ";
                        cin >> changeChoice;

                        dataWild[changeChoice - 1] = changeParam(dataWild[changeChoice - 1]);
                        break;
                    default:
                        cout << "Error! Invalid input!\n";
                }
            case 4:
                std::cout << "Exit...\n";
                system("pause");
                return 0;
            default:
                std::cout << "Invalid input!\n";
        }
    }
}

template <class C>
C enter(C _animals) {
    unsigned valueU = 0;

    cout << "Enter what animal do you want to add: ";
    cin >> animals.animal;
    _animals.setAnimal(animals.animal);

    cout << "Name: ";
    cin >> animals.name;
    _animals.setName(animals.name);

    while (valueU > 2 || valueU < 1) {
        cout << "Sex: \n"
             << "     1) Male\n"
             << "     2) Female\n";
        cin >> valueU;
        switch (valueU) {
            case 1:
                _animals.setSex(1);
                break;
            case 2:
                _animals.setSex(0);
                break;
            default:
                cout << "Error!\n";
        }
    }

    cout << "Price: ";
    cin >> animals.price;
    _animals.setPrice(animals.price);

    cout << "\n";

    return _animals;
}

template <class C>
void print(C _animal) {
    cout << "  Animal: " << _animal.getAnimal() << "\n"
         << "  Name: " << _animal.getName() << "\n"
         << "  Sex: ";
    if (_animal.getSex())
        cout << "male";
    else
        cout << "female";
    cout << "\n"
         << "  Price: $" << _animal.getPrice() << "\n\n";
}

template <class C>
C changeParam(C _animal) {
    cout << "New name: ";
    cin >> animals.name;

    cout << "New price: ";
    cin >> animals.price;

    cout << "\n";

    return _animal;
}