#ifndef PETS_H
#define PETS_H

#include "Zoo.h"

class Pets : virtual public Zoo {
private:
    static unsigned quantity;

public:
    Pets();
    Pets(string _animal, string _name, bool _sex, double _price);
    Pets(const Pets &object);
    ~Pets();

    inline unsigned getQuantity() { return quantity; };
    void counter();

    inline void setType(bool _type) { Zoo::type = _type; };
};

#endif //PETS_H
