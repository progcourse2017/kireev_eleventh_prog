#include "Pets.h"

unsigned Pets::quantity = 0;

Pets::Pets() {
    setType(0);
    counter();
}

Pets::Pets(string _animal, string _name,  bool _sex, double _price):
        Zoo(_animal, _name, _sex, _price) {
    setType(0);
    counter();
}

Pets::Pets(const Pets &object):
        Zoo(object) {
    quantity = object.quantity;
}

Pets::~Pets() {}

void Pets::counter() {
    Pets::quantity++;
    Zoo::allCount++;
}